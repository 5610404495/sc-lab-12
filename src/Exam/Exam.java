package Exam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Exam {
	private String[] list = new String[2];
	private HashMap<String,Double> score = new HashMap<String, Double>();
	private ArrayList<String> name = new ArrayList<String>();
	private BufferedReader in ;
	public Exam(String txt) {
		 try {
			in = new BufferedReader(new FileReader(txt));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("File not Founded.");
		}
	}
	public void findAvg() {
		try {
			String str;
			Double avg;
			while ((str = in.readLine() ) != null) {
				 list = str.split(", ");
				 avg = (Double.parseDouble(list[1])+Double.parseDouble(list[2]))/2;
				 score.put(list[0], avg);
				 name.add(list[0]);
			}
			
		}
		

		catch (IOException e ) {
			System.err.println("File not Founded.");
		}
	}
	public void CreateFile() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("average.txt", true));
			System.out.println("//Write This content");
			writer.write("\n<<<<Exam Score>>>>\n");
			System.out.println("<<<<Exam Score>>>>");
			System.out.println("Name \t Average");
			writer.write("Name \t Average\n");
			System.out.println("==== \t =======");
			writer.write("==== \t =======\n");
			for (String n : name)
			{
				System.out.println(n +"\t"+score.get(n));
				writer.write(n +"\t"+score.get(n)+"\n");
			}
			System.out.println("//Done.");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("File not Founded.");
		}
		
	}
}
